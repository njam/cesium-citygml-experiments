cesium-citygml-experiments
==========================

Visualizing CityGML buildings with Cesium.


About
-----

There's a NodeJS script using [citygml-to-3dtiles](https://github.com/njam/citygml-to-3dtiles) to convert the [CityGML](https://www.citygml.org/) data to [3D Tiles](https://github.com/AnalyticalGraphicsInc/3d-tiles).
A Webpack configuration then creates a static website loading the 3D Tiles in [Cesium](https://cesiumjs.org/) on top of OpenStreetMap tiles. 

Demo: https://njam.gitlab.io/cesium-citygml-experiments/

CityGML data by [*Stadt Zürich, Geomatik Vermessung*](https://www.stadt-zuerich.ch/ted/de/index/geoz.html),
available on the [*Stadt Zürich Open Data catalog*](https://data.stadt-zuerich.ch/dataset?q=&tags=3d-stadtmodell&sort=score+desc%2C+date_last_modified+desc).

![screenshot](docs/screenshot.png)


Development
-----------

Install dependencies:
```
npm install
```

Run the conversion from CityGML to 3D Tiles:
```
npm run convert
```

Build the website and serve it with auto-reload on http://localhost:8080/:
```
npm start
```

Or build a "production" bundle in `dist/`:
```
npm run-script build
```
