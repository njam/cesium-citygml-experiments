import 'cesium/Widgets/widgets.css';
import './css/index.css';
import Promise from 'promise';

main();

async function main() {
  const Cesium = await import(/* webpackChunkName: "cesium" */ 'cesium/Cesium');

  Cesium.Camera.DEFAULT_VIEW_RECTANGLE = Cesium.Rectangle.fromDegrees(8.5, 47.35, 8.55, 47.4);
  Cesium.Camera.DEFAULT_VIEW_FACTOR = 0;
  Cesium.Ion.defaultAccessToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI3MjI2ZDM2YS0yY2M4LTRhMjctYjkyZS1kYzY3M2Q4OGRkMDEiLCJpZCI6NTA4OCwic2NvcGVzIjpbImFzciIsImdjIl0sImlhdCI6MTU0MjM1MjE5N30.B4q3Rqg6GyTN9mpndm30U-wJO1FVt0_YxPpN17_6utY';

  const viewer = new Cesium.Viewer('cesiumContainer', {
    selectionIndicator: false,
    baseLayerPicker: false,
    fullscreenButton: false,
    sceneModePicker: false,
    timeline: false,
    animation: false,
    imageryProvider: Cesium.createOpenStreetMapImageryProvider({
      url: 	'https://api.maptiler.com/maps/basic-v2/',
      fileExtension: 'png?key=ViAb0aj9toulGzf2BDqw',
      credit: '<a href="https://www.maptiler.com/copyright/" target="_blank">&copy; MapTiler</a> <a href="https://www.openstreetmap.org/copyright" target="_blank">&copy; OpenStreetMap contributors</a>',
    }),
    terrainProvider: Cesium.createWorldTerrain({
      requestWaterMask: true, // required for water effects
      requestVertexNormals: true // required for terrain lighting
    }),
  });
  //viewer.scene.globe.enableLighting = true;
  viewer.scene.globe.depthTestAgainstTerrain = true;


  // Add tileset
  const tileset = new Cesium.Cesium3DTileset({
    url: './data/3dtiles/zurich-lod2-lochergut600/tileset.json',
  });
  const city = viewer.scene.primitives.add(tileset);

  // Adjust the tileset height
  // Tiles are based on "LN02", while terrain is based on "WGS84"-ellpisoid
  const heightOffset = +52;
  city.readyPromise.then(function(tileset) {
    const boundingSphere = tileset.boundingSphere;
    const cartographic = Cesium.Cartographic.fromCartesian(boundingSphere.center);
    const surfacePosition = Cesium.Cartesian3.fromRadians(cartographic.longitude, cartographic.latitude, 0.0);
    const offsetPosition = Cesium.Cartesian3.fromRadians(cartographic.longitude, cartographic.latitude, heightOffset);
    const translation = Cesium.Cartesian3.subtract(offsetPosition, surfacePosition, new Cesium.Cartesian3());
    tileset.modelMatrix = Cesium.Matrix4.fromTranslation(translation);
  });

  // Zoom to tileset once everything is loaded
  Promise.all([
    tileset.readyPromise,
    viewer.terrainProvider.readyPromise
  ]).then(() => {
    viewer.zoomTo(tileset, new Cesium.HeadingPitchRange(1.0, -1, 400));
  });

  // Tooltip showing the 'properties' of tileset features
  const tooltip = createTooltip();
  viewer.container.appendChild(tooltip);
  viewer.screenSpaceEventHandler.setInputAction(function(movement) {
    // Pick a new feature
    const pickedFeature = viewer.scene.pick(movement.endPosition);
    if (!Cesium.defined(pickedFeature)) {
      tooltip.style.display = 'none';
      return;
    }

    let text = '-no properties-';
    let properties = pickedFeature.getPropertyNames();
    if (properties.length > 0) {
      let lines = properties.map(property => {
        let value = pickedFeature.getProperty(property);
        return `${property}: ${value}`;
      });
      text = lines.join("\n");
    }

    tooltip.style.display = 'block';
    tooltip.style.bottom = viewer.canvas.clientHeight - movement.endPosition.y + 'px';
    tooltip.style.left = movement.endPosition.x + 'px';
    tooltip.innerText = text;
  }, Cesium.ScreenSpaceEventType.MOUSE_MOVE);

  // Colo-code buildings based on the square root of their volume
  city.style = new Cesium.Cesium3DTileStyle({
    color: 'hsl(min(sqrt(Number(${volume})/75000)*0.3,0.3), 1.0, 0.5)'
  });

}

function createTooltip() {
  let tooltip = document.createElement('div');
  tooltip.className = 'backdrop';
  tooltip.style.display = 'none';
  tooltip.style.position = 'absolute';
  tooltip.style.bottom = '0';
  tooltip.style.left = '0';
  tooltip.style['pointer-events'] = 'none';
  tooltip.style.padding = '4px';
  tooltip.style.backgroundColor = 'black';
  tooltip.style.color = 'white';
  return tooltip;
}
