import Converter from 'citygml-to-3dtiles'
import Cesium from 'cesium'

async function main () {
  let lochergut = Cesium.Cartesian3.fromDegrees(8.5177282, 47.3756023)

  let converter = new Converter({
    propertiesGetter: (cityObject, properties) => {
      let mesh = cityObject.getTriangleMesh()
      return {
        volume: mesh.getVolume(),
        footprint: mesh.getSurfaceArea()
      }
    },
    objectFilter: (cityObject) => {
      let distance = Cesium.Cartesian3.distance(lochergut, cityObject.getAnyPoint())
      return distance < 600
    }
  })

  await converter.convertFiles('data/citygml/zurich-lod1-full/', 'data/3dtiles/zurich-lod1-lochergut600/')
}

main()
